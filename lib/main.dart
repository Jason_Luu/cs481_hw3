import 'package:flutter/material.dart';


void main() {
  runApp(MyApp());
}


class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context){
    Color color = Theme.of(context).primaryColor;
    Widget emotionSection = Container(
        child: Row(
          mainAxisAlignment:  MainAxisAlignment.spaceEvenly,
          children: [
            LikeWidget(),
            LoveWidget()
          ],
        )
    );
    Widget aboutSection = Container(
        padding: EdgeInsets.all(32),
        child: Text(
          'About the place:\n'
              '\tName: Golden Gate Bridge\n'
              '\tLocation: US City of San Francisco, California\n'
              '\tDesigner: Engineer Joseph Strauss\n'
              '\tYear Built: 1917\n'
              '\tYear of opening: 1937\n'
              '\tHeight: 746 feet\n'
              '\tLength: about 1.7 miles\n'
              '\tWidth: 90 feet\n'
        ),
    );
    return MaterialApp(
        title:'Stateful Object Homework',
        home: Scaffold(
          appBar: AppBar(
            title: Text('HW3 Stateful Object Homework'),
          ),
          body: ListView(
            children: [
              Image.asset(
                'images/GoldenGateBridge(1).jpg',
                fit: BoxFit.cover,
              ),
              emotionSection,
              aboutSection,
            ],
          ),

        )
    );
  }
}//MyApp

class LikeWidget extends StatefulWidget{
  @override
  _LikeWidgetState createState() => _LikeWidgetState();
}
class LoveWidget extends StatefulWidget{
  @override
  _LoveWidgetState createState() => _LoveWidgetState();
}
class _LikeWidgetState extends State<LikeWidget>{
  bool _isLiked = true;
  int _likedCount = 0;
  @override
  Widget build(BuildContext context){
    Color color = Theme.of(context).primaryColor;
    return Column(
      mainAxisSize: MainAxisSize.min,
      children:[
        SizedBox(
            width: 10,
            child: Container(
              margin: const EdgeInsets.only(top:10),
              child: Text(
                  '$_likedCount',
                style: TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.w400,
                  color:color,
                ),
              ),

            )
        ),
        Container(
          padding: EdgeInsets.all(0),
          child: IconButton(
            icon: (_isLiked ? Icon(Icons.thumb_up) : Icon(Icons.thumb_up)),
            onPressed: _toogleLike,
            color: color,
          )
        ),


      ]
    );
  }
  void _toogleLike(){
    setState(() {
      if(_isLiked){
        _likedCount--;
        _isLiked = false;
      }
      else{
        _likedCount++;
        _isLiked = true;
      }
    });

  }
}

class _LoveWidgetState extends State<LoveWidget> {
  bool _isLoved = true;
  int _LovedCount = 0;
  @override
  Widget build(BuildContext context){
    Color color = Theme.of(context).primaryColor;
    return Column(
        mainAxisSize: MainAxisSize.min,
        children:[
          SizedBox(
              width: 10,
              child: Container(
                margin: const EdgeInsets.only(top:10),
                child: Text(
                  '$_LovedCount',
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                    color:color,
                  ),
                ),

              )
          ),
          Container(
              padding: EdgeInsets.all(0),
              child: IconButton(
                icon: (_isLoved ? Icon(Icons.favorite_border) : Icon(Icons.favorite)),
                onPressed: _toogleLove,
                color: color,
              )
          ),


        ]
    );
  }
  void _toogleLove(){
    setState(() {
      if(_isLoved){
        _LovedCount--;
        _isLoved = false;
      }
      else{
        _LovedCount++;
        _isLoved = true;
      }
    });

  }
}


